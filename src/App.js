import React, { Component } from 'react';
import './App.css';
import TemperatureBar from "./TemperatureBar";
import { Button } from "react-bootstrap";
class App extends Component {
  constructor() {
    super();
    this.state = {
      startTest: false,
      beerVarieties: [
          {   
              name: 'Beer1',
              temperature: 2.0,
              thresholdLow: 4,
              thresholdHigh: 6
          },
          {   
              name: 'Beer2',
              temperature: 2.2,
              thresholdLow: 5,
              thresholdHigh: 6
          },
          {   
              name: 'Beer3',
              temperature: 3.2,
              thresholdLow: 4,
              thresholdHigh: 7
          },
          {   
              name: 'Beer4',
              temperature: 6.8,
              thresholdLow: 6,
              thresholdHigh: 8
          },
          {   
              name: 'Beer5',
              temperature: 9.0,
              thresholdLow: 3,
              thresholdHigh: 5
          }
      ]
    }
  }

  componentWillUnmount () {
    if(this.timer) {
      clearInterval(this.timer);
    }
  }
  _getAllBeerTemperature(){
    return this.state.beerVarieties.map((beer, index) => {
      const {temperature, thresholdLow, thresholdHigh} = beer;

      const color = function(){
        if((temperature >= thresholdLow) && (temperature <= thresholdHigh)) {
          return 'success';
        } else if (temperature < thresholdLow) {
          return 'info';
        } else if (temperature > thresholdHigh) {
          return 'danger';
        }
      }

      return (
        <TemperatureBar key={index} color={color()} beer={beer} thresholdLow={thresholdLow} thresholdHigh={thresholdHigh} />
      )
    
    })
  }
  //Test the solution
  _testSoloution = () => {
    this.setState({
      startTest: true
    })
    this.timer = setInterval(() => {
      for(let i = 0; i < this.state.beerVarieties.length; i++){
        const newTemperature = Math.round(Math.random() * 100)/10;
        this.setState({
          beerVarieties: [
              ...this.state.beerVarieties.slice(0,i),
              {
                  ...this.state.beerVarieties[i],
                  temperature: newTemperature
              },
              ...this.state.beerVarieties.slice(i + 1),
          ]
        })
      }  
    },3000);
  }
  //Stop testing
  _stopTest = () => {
    this.setState({
      startTest: false
    })
    clearInterval(this.timer);
  }
  render() {
    return (
      <div className="App">
        <div className="container">
          <h4 style={{marginBottom: 50}}>Thermometer for refrigerated truck</h4>
          {this._getAllBeerTemperature()}
        </div>
        {this.state.startTest ? <Button bsStyle="warning" onClick={this._stopTest}>Stop testing</Button> : <Button bsStyle="primary" onClick={this._testSoloution}>Start automated tests</Button>}
      </div>
    );
  }
}

export default App;
