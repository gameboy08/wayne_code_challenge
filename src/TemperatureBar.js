import React from "react";
import { ProgressBar } from "react-bootstrap";
const TemperatureBar = (props) => {
    const {beer, color} = props;
    const {name, temperature, thresholdLow, thresholdHigh} = beer;
    return (
        <div className='temperature-bar-container'>
            <div className="scale-step" style={{marginLeft: `${thresholdLow*10}%`}}>
                <div>{thresholdLow}°C</div>
                <div className="label-line"></div>
            </div>
            <div className="scale-step" style={{marginLeft: `${thresholdHigh*10}%`}}>
                <div>{thresholdHigh}°C</div>
                <div className="label-line"></div>
            </div>
            <ProgressBar bsStyle={color} now={temperature * 10} label={`${temperature}°C`} />
            <div>
                {name} {color === 'danger' || color === 'info' ? <span className="alert-text">Alert! The temperature is too {color === 'danger' ? 'high' : 'low'}</span> : ''}
            </div>
        </div>
    )
}

export default TemperatureBar;