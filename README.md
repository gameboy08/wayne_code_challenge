## The Brewery - Transport Refrigeration Sensors
The app is developed as a solution using ReactJS for the code chanlenge "*The Brewery*" from Tabcorp.

## Author
Wayne Li

## Getting Started
#### Using npm to install dependencies and start the project
```
npm install
npm start
```

## Explanations
The Five varieties of beer's temperature and storing temperature range are kept in the state of `App` component. Five thermometer bars are corresponding to those five different beers. Each thermometer has its labeled standard range. I used light blue, red and green bar to corresponds to below than, higher than and in the standard temperature range. Therefore, the driver should be cautious when he/she saw blue and red color. There also texts below each thermometer bar to show alerting message dependly depending on the current situation. When you click the *Start automated tests* button, it will simulate that when the sensors input current temperature, and then the view keep displaying the changes.

The relevant things of each thermometer bar are kept in another component, so it's easy to use this component repeatedly if there are more varieties of beers to come. Also, the state of each beer is updated immutably.

For the improvements, I think the scale can be divided more finely. 

